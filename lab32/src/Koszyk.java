// **********************************************************************
//   Koszyk.java
//
//   Reprezentuje koszyk zakupowy jako tablica przedmiotow (Items)
// **********************************************************************


import java.text.NumberFormat;

public class Koszyk
{

    private int iloscRzeczy;       // ilosc rzeczy w koszyku
    private double cenaCalkowita;  // calkowita cena przedmiotow w koszyku
    private int pojemnosc;         // biezaca pojemnosc koszyka
    Item[] koszyk;					//tablica obiektow 'Item'
    // -----------------------------------------------------------
    // tworzy pusty koszyk o pojemnosci 5 przedmiotow.
    // -----------------------------------------------------------
    public Koszyk()
    {
	pojemnosc = 5;
	iloscRzeczy = 0;
	cenaCalkowita = 0.0;
	koszyk = new Item[pojemnosc];
    }

    // -------------------------------------------------------
    //  Dodaje rzecz do koszyka.
    // -------------------------------------------------------
    public void dodajDoKoszyka(String nazwaRzeczy, double cena, int ilosc)
    {
    	if(iloscRzeczy == pojemnosc) powiekszRozmiar();
    	koszyk[iloscRzeczy++] = new Item(nazwaRzeczy, cena, ilosc);
    	cenaCalkowita += ilosc*cena;
    }

    // -------------------------------------------------------
    //  Zwraca zawartosc koszyka wraz z podsumowaniem.
    // -------------------------------------------------------
    public String toString()
    {
	NumberFormat fmt = NumberFormat.getCurrencyInstance();

	String zawartosc = "\nKoszyk\n";
	zawartosc += String.format("%-15s%-8s%-8s%-15s\n", "Nazwa", "Cena", "ilosc", "Cena calkowita");

	for (int i = 0; i < iloscRzeczy; i++)
	    zawartosc += koszyk[i].toString() + "\n";

	zawartosc += "\nTotal Price: " + fmt.format(cenaCalkowita);
	zawartosc += "\n";

	return zawartosc;
    }

    // ---------------------------------------------------------
    //  Zwieksza pojemnosc koszyka o 3
    // ---------------------------------------------------------
    private void powiekszRozmiar()
    {
    	Item[] tmp = koszyk;
    	koszyk = new Item[++pojemnosc];
    	System.arraycopy(tmp,0,koszyk,0,tmp.length);
    }

}

