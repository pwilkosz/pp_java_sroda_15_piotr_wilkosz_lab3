// ***************************************************************
//   Zakupy.java
//
//   Wykorzystuje klase Item do stworzenia przedmiotów i dodania ich do koszyka
//   przechowywanego w ArrayList.
// ***************************************************************

import java.util.ArrayList;

import cs1.Keyboard;

public class Zakupy
{
    public static void main (String[] args)
    {
	ArrayList<Item> koszyk = new ArrayList<Item>();

	Item rzecz;
	String nazwaRzeczy;
	double cenaRzeczy;
	int ilosc;

	String kontynuujZakupy = "t";

	do 
	    {
		System.out.print ("Podaj nazwe rzeczy: "); 
		nazwaRzeczy = Keyboard.readString();

		System.out.print ("Podaj cene jednostkowa: ");
		cenaRzeczy = Keyboard.readDouble();

		System.out.print ("Podaj ilosc: ");
		ilosc = Keyboard.readInt();

		// *** stworz nowa rzecz i dodaj ja do koszyka
		koszyk.add(new Item(nazwaRzeczy, cenaRzeczy, ilosc));

		// *** wypisz zawartosc koszyka z zastosowaniem println
		
		//System.out.println(koszyk);
		//uzycie metody toString spowodowalo wyswietlenie wszystkich produktow w jednej linii, 
		//dlatego postanowilen uzyc iteratora do wyswietlenia wszystkich elementow
		System.out.println(String.format("%-15s%-8s%-8s%-15s", "Nazwa", "Cena", "ilosc", "Cena calkowita"));
		for(Item prod: koszyk){
			System.out.println(prod.toString());
		}
		System.out.print ("Kontynuowac zakupy (t/n)? ");
		kontynuujZakupy = Keyboard.readString();
	    }
	while (kontynuujZakupy.equals("t"));

    }
}

